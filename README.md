# Setup for a Certificate Authority that can be used to issue TLS certs for development

## Problem

You want to test your code using HTTPS, but on a development machine that
is behind the firewall, or perhaps just a little Linux container on an
ephemeral IP address.

If you just create a self-signed TLS cert, than all your client-side code
(like if your app performs HTTPS GET) will fail with an "insecure" error
because it doesn't have an authoritative CA in the certificate path.

## Solution

* Set up your own certificate authority (CA)
* Issue your own TLS certificates from it
* Add the certificate authority to your development systems's
  root certificates

## One-time setup

1. Clone this repo, and `cd` into the top-level directory

1. Install `openssl` (e.g. `sudo pacman -S openssl`)

1. Edit

1. Create the CA's root keypair and protect it:

   ```
   % openssl genrsa -out private/my-development-ca.key.pem 4096
   % chmod 400 private/my-development-ca.key.pem
   ```

   Given that this is never going to be used for real-world applications,
   just for development, you may not need to protect this keypair very much.
   Don't do this on your main work machine, just inside a VM or a container.

1. Create the root certificate:

   ```
   % openssl req -config openssl.cnf -key private/my-development-ca.key.pem \
        -new -x509 -days 3650 -sha256 -extensions v3_ca \
        -out certs/my-development-ca.cert.pem
   ```

   When prompted, for "Common Name", use a plausible domain name, like
   `my-development-ca.example.com`.

1. Install the root certificate into your operating system's
   default certificate store. On UBOS, or Arch:

   ```
   % sudo trust anchor --store certs/my-development-ca.cert.pem

## For each certificate you need


For each separate certificate you need, do the following.
Assume the hostname for the certificate is `test.example.com`
and replace this string appropriately.

1. Generate a keypair:

   ```
   % openssl genrsa -out private/test.example.com.key.pem 2048
   % chmod 400 private/test.example.com.key.pem
   ```


1. Generate the certificate request:

   ```
   % openssl req -config openssl.cnf \
        -key private/test.example.com.key.pem \
        -new -out csr/test.example.com.csr.pem
   ```

   Enter the hostname for which this certificate will be used.
   Note that DNS has to be set up suitably that your code actually
   accesses your development server with this DNS hostname.

1. Generate the certificate from the certificate request:

   ```
   % openssl ca -config openssl.cnf \
        -extensions server_cert -notext -md sha256 \
        -in csr/test.example.com.csr.pem \
        -out certs/test.example.com.cert.pem
   % chmod 444 certs/test.example.com.cert.pem
   ```

1. To use on UBOS, when creating a Site with `ubos-admin createsite`,
   specify option `--tls` and enter `private/test.example.com.key.pem`
   as the key, and `certs/test.example.com.cert.pem` as the
   certificate file. Leave the other certificate-related files questions
   blank.

## Sources:

* https://jamielinux.com/docs/openssl-certificate-authority/
